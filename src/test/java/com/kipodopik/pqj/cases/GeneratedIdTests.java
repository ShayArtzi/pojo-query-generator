package com.kipodopik.pqj.cases;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.kipodopik.pqj.PojoQueryGenerator;
import com.kipodopik.pqj.entity.generated.ApiTokenEntityGeneratedId;
import com.kipodopik.pqj.exception.PqjException;

public class GeneratedIdTests {
		
	@Test
	public void getenratedIdInsertStmtTest() throws PqjException {		
		PojoQueryGenerator<ApiTokenEntityGeneratedId> pqg = new PojoQueryGenerator<>(ApiTokenEntityGeneratedId.class);		
		assertEquals( "INSERT INTO API_TOKEN (NICKNAME, TOKEN, ACTIVE) VALUES (?, ?, ?)", pqg.generateInsert() );		
	}

}
