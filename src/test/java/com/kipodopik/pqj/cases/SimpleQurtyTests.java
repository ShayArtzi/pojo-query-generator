package com.kipodopik.pqj.cases;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.kipodopik.pqj.PojoQueryGenerator;
import com.kipodopik.pqj.entity.simple.ApiTokenEntity;
import com.kipodopik.pqj.exception.PqjException;

public class SimpleQurtyTests { 

	@Test
	public void simpleSelectStmtTest() throws PqjException {		
		PojoQueryGenerator<ApiTokenEntity> pqg = new PojoQueryGenerator<>(ApiTokenEntity.class);		
		assertEquals( "SELECT API_TOKEN_ID, NICKNAME, TOKEN, ACTIVE FROM API_TOKEN", pqg.generateSelect() );		
	}
	
	@Test
	public void simpleInsertStmtTest() throws PqjException {		
		PojoQueryGenerator<ApiTokenEntity> pqg = new PojoQueryGenerator<>(ApiTokenEntity.class);		
		assertEquals( "INSERT INTO API_TOKEN (API_TOKEN_ID, NICKNAME, TOKEN, ACTIVE) VALUES (?, ?, ?, ?)", pqg.generateInsert() );		
	}
	
	@Test
	public void simpleUpdateByIdStmtTest() throws PqjException {		
		PojoQueryGenerator<ApiTokenEntity> pqg = new PojoQueryGenerator<>(ApiTokenEntity.class);		
		assertEquals( "UPDATE API_TOKEN SET NICKNAME = ?, TOKEN = ?, ACTIVE = ? WHERE API_TOKEN_ID = ?", pqg.generateUpdateById() );		
	}
	
}
