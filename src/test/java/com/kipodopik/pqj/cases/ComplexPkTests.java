package com.kipodopik.pqj.cases;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.kipodopik.pqj.PojoQueryGenerator;
import com.kipodopik.pqj.entity.complex.AccountEntity;
import com.kipodopik.pqj.exception.PqjException;

public class ComplexPkTests {

	@Test	
	public void complexSelectStmtTest() throws PqjException {		
		PojoQueryGenerator<AccountEntity> pqg = new PojoQueryGenerator<>(AccountEntity.class);		
		assertEquals( 
			"SELECT DATE, ACCOUNT_ID, BANK_NUM, BRANCH_NUM, ACCOUNT_NUM FROM ACCOUNT", 
			pqg.generateSelect() );		
	}
	
	@Test
	public void complexInsertStmtTest() throws PqjException {		
		PojoQueryGenerator<AccountEntity> pqg = new PojoQueryGenerator<>(AccountEntity.class);		
		assertEquals( 
			"INSERT INTO ACCOUNT (DATE, ACCOUNT_ID, BANK_NUM, BRANCH_NUM, ACCOUNT_NUM) VALUES (?, ?, ?, ?, ?)", 
			pqg.generateInsert() );		
	}
	
	@Test	
	public void complexUpdateByIdStmtTest() throws PqjException {		
		PojoQueryGenerator<AccountEntity> pqg = new PojoQueryGenerator<>(AccountEntity.class);		
		assertEquals( 
			"UPDATE ACCOUNT SET BANK_NUM = ?, BRANCH_NUM = ?, ACCOUNT_NUM = ? WHERE DATE = ? AND ACCOUNT_ID = ?", 
			pqg.generateUpdateById() );		
	}
	
}
