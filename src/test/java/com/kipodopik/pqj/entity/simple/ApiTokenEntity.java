package com.kipodopik.pqj.entity.simple;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "API_TOKEN")
public class ApiTokenEntity implements Serializable {

	private static final long serialVersionUID = 8319424160382553200L;

	@Id	
	@Column(name = "API_TOKEN_ID")
	private Long apiTokenId;
	
	@Column(name = "NICKNAME")
	private String nickname;
	
	@Column(name = "TOKEN")
	private String token;
	
	@Column(name = "ACTIVE")
	private Integer active;
		
}
