package com.kipodopik.pqj.entity.complex;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class AccountPk implements Serializable {

	private static final long serialVersionUID = 7123786965125087266L;

	@Column(name = "DATE")
	@Temporal(TemporalType.DATE)
	private Date date;
		
	@Column(name = "ACCOUNT_ID")
	private long accountId;
	
	public AccountPk() {
		
	}
	
	public AccountPk(Date date, long accountId) {	
		this.date = date;
		this.accountId = accountId;
	}
}
