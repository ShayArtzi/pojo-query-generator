package com.kipodopik.pqj.entity.complex;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="ACCOUNT")
public class AccountEntity implements Serializable {

	private static final long serialVersionUID = 5758648971414302508L;
	
	@EmbeddedId
	private AccountPk id;

	@Column(name = "BANK_NUM")
	private Long bankNum;

	@Column(name = "BRANCH_NUM")
	private Integer branchNum;

	@Column(name = "ACCOUNT_NUM")
	private Long accountNum;	
	
}
