package com.kipodopik.pqj.entity.generated;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "API_TOKEN")
public class ApiTokenEntityGeneratedId implements Serializable {

	private static final long serialVersionUID = 8319424160382553201L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "API_TOKEN_ID")
	private Long apiTokenId;
	
	@Column(name = "NICKNAME")
	private String nickname;
	
	@Column(name = "TOKEN")
	private String token;
	
	@Column(name = "ACTIVE")
	private Integer active;
	
}
