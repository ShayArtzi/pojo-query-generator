package com.kipodopik.pqj.exception;

public class PqjException extends Exception {
	
	private static final long serialVersionUID = -5785909707543980460L;
	
	public PqjException(String msg) {
		super(msg);
	}

}
