package com.kipodopik.pqj;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kipodopik.pqj.exception.PqjException;
import com.kipodopik.pqj.util.ListUtil;
import com.kipodopik.pqj.util.ReflectionUtil;
import com.kipodopik.pqj.util.StringUtil;



public class PojoQueryGenerator<T> {

	private Class<T> entityClass;
	
	public PojoQueryGenerator(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	public String generateSelect() throws PqjException {		
		validateEntity();		
		String tableName = getTableName();		
		
		List<String> fieldNames = getFieldNames(FieldGroup.ALL);		
		Field embeddedIdField = ReflectionUtil.getFieldByAnnotation(entityClass, EmbeddedId.class);
		if (embeddedIdField != null) {
			Class<?> idClass = embeddedIdField.getType();			
			
			List<String> idFieldNames = 
					ReflectionUtil.getAnnotationsForFields(idClass, Column.class)
					.stream().map( c -> c.name() ).collect( Collectors.toList() );
			
			fieldNames = ListUtil.join(idFieldNames, fieldNames);
			/*
			Collections.reverse(idFieldNames);
			for (String idFieldName : idFieldNames) {
				fieldNames.add(0, idFieldName);
			}
			*/
		}
		
		
		return generateSelectFromFieldsAndTable(fieldNames, tableName);		
	}

	public String generateInsert() throws PqjException {		
		validateEntity();		
		String tableName = getTableName();
		
		Field embeddedIdField = ReflectionUtil.getFieldByAnnotation(entityClass, EmbeddedId.class);

		List<String> fieldNames = new ArrayList<String>();
		
		if (embeddedIdField == null) {			
			FieldGroup fg = ReflectionUtil.containsFieldWithAnnotation(entityClass, GeneratedValue.class) ? FieldGroup.NON_ID : FieldGroup.ALL;				
			fieldNames.addAll( getFieldNames(fg) );
		} else {
			Class<?> idClass = embeddedIdField.getType();			
			
			List<String> idFieldNames = 
					ReflectionUtil.getAnnotationsForFields(idClass, Column.class)
					.stream().map( c -> c.name() ).collect( Collectors.toList() );
					
			
			fieldNames.addAll(idFieldNames);
			fieldNames.addAll( getFieldNames(FieldGroup.ALL) );			
		}
		
		return generateInsertFromFieldsAndTable(fieldNames, tableName);		
	}	
	
	public String generateUpdateById() throws PqjException {
		validateEntity();		
		String tableName = getTableName();		
		
		Field embeddedIdField = ReflectionUtil.getFieldByAnnotation(entityClass, EmbeddedId.class);
		
		List<String> idFields;
		List<String> nonIdFields;
		
		if (embeddedIdField == null) {	
			idFields = getFieldNames(FieldGroup.ID);		
			nonIdFields = getFieldNames(FieldGroup.NON_ID);			
		} else {
			Class<?> idClass = embeddedIdField.getType();
			
			idFields = 
				ReflectionUtil.getAnnotationsForFields(idClass, Column.class)
				.stream().map( c -> c.name() ).collect( Collectors.toList() );
			nonIdFields = getFieldNames(FieldGroup.NON_ID);
		}
		
		return generateUpdateByIdFromFieldsAndTable(idFields, nonIdFields, tableName);
	}
	
	private String generateSelectFromFieldsAndTable(List<String> fieldNames, String tableName) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");		
		sb.append( StringUtil.join(fieldNames, ", ") );		
		sb.append(" FROM ");
		sb.append(tableName);
		return sb.toString();
	}

	private String generateInsertFromFieldsAndTable(List<String> fieldNames, String tableName) {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO ");
		sb.append(tableName);		
		sb.append(" (");		
		sb.append( StringUtil.join(fieldNames, ", ") );				
		sb.append(") VALUES (");		
		List<String> questionMarks = fieldNames.stream().map( f -> "?" ).collect( Collectors.toList() );
		sb.append( StringUtil.join(questionMarks, ", ") );		
		sb.append(")");
		return sb.toString();
	}
	
	private String generateUpdateByIdFromFieldsAndTable(
			List<String> idFields, List<String> nonIdFields, String tableName) {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE ");
		sb.append(tableName);		
		sb.append(" SET ");		
		
		List<String> nonIdWithQuestionMark = nonIdFields.stream().map( f -> f + " = ?" ).collect( Collectors.toList() );		
		sb.append( StringUtil.join(nonIdWithQuestionMark, ", ") );
		sb.append(" WHERE ");
		
		List<String> idWithQuestionMark = idFields.stream().map( f -> f + " = ?" ).collect( Collectors.toList() );
		sb.append( StringUtil.join(idWithQuestionMark, " AND ") );

		return sb.toString();
	}

	
	
	private String getTableName() throws PqjException {
		Table tableAnnotation = entityClass.getAnnotation(Table.class);
		if (tableAnnotation == null) {
			throw new PqjException(entityClass.getName() + " is not a table (It does not have the @Table annotation)");			
		}
		String tableName = tableAnnotation.name();
		if (tableName == null) {
			throw new PqjException(entityClass.getName() + ": table name undefined in the @Table annotation");
		}
		return tableName;
	}
	
	private List<String> getFieldNames(FieldGroup fieldGroup) {
		
		Field[] fields = entityClass.getDeclaredFields();
		List<String> fieldNames = new ArrayList<String>();
		for (Field field : fields) {
			
			Id idAnnotation = field.getAnnotation(Id.class);
			Column columnAnnotation = field.getAnnotation(Column.class);
			
			if (columnAnnotation == null) {
				continue;
			}
								
			switch (fieldGroup) {
			case ALL:
				fieldNames.add( columnAnnotation.name() );				
				break;
			case ID:
				if (idAnnotation != null) {
					fieldNames.add( columnAnnotation.name() );
				}		
				break;
			case NON_ID:				
				if (idAnnotation == null) {
					fieldNames.add( columnAnnotation.name() );
				}
				break;
			}
			
			
		}
		
		return fieldNames;
		
	}
	
	private void validateEntity() throws PqjException {
		if ( !ReflectionUtil.containsClassLevelAnnotation(entityClass, Entity.class) ) {
			throw new PqjException(entityClass.getName() + " is not an entity (It does not have the @Entity annotation)");
		}
		
	}
	
	
}
