package com.kipodopik.pqj.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReflectionUtil {

	public static Field getFieldByAnnotation(Class<?> cls, Class<? extends Annotation> annotationClass) {
		Field[] fields = cls.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(annotationClass) != null) {
				return field;
			}
		}
		return null;
	}
	
	@SafeVarargs
	public static List<Field> getFieldsByAnnotation(Class<?> cls, Class<? extends Annotation> ...annotations) {
		Field[] fields = cls.getDeclaredFields();
		List<Field> filteredFields = new ArrayList<Field>();
				
		for (Field field : fields) {			
			for (Class<? extends Annotation> annotation : annotations) {
				if (field.getAnnotation(annotation) != null) {
					filteredFields.add(field);
					break;
				}
			}
		}
		
		return filteredFields;
	}
	
	
	public static <T extends Annotation> List<T> getAnnotationsForFields(Class<?> cls, Class<T> annotation) {
		Field[] fields = cls.getDeclaredFields();
		List<T> rv = new ArrayList<T>();
				
		for (Field field : fields) {
			T actualAnnotaion = field.getAnnotation(annotation);
			if (actualAnnotaion != null) {
				rv.add(actualAnnotaion);				
			}			
		}
		
		return rv;
	}
	
	public static boolean containsFieldWithAnnotation(Class<?> cls, Class<? extends Annotation> annotationClass) {
		return getFieldByAnnotation(cls, annotationClass) != null;		
	}
	
	public static <T extends Annotation> T getClassAnnotation(Class<?> cls, Class<T> annotationClass) {
		return cls.getAnnotation(annotationClass);
	}
	
	public static boolean containsClassLevelAnnotation(Class<?> cls, Class<? extends Annotation> annotationClass) {
		return getClassAnnotation(cls, annotationClass) != null;
	}
	
}
