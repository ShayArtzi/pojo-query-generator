package com.kipodopik.pqj.util;

import java.util.List;
import java.util.StringJoiner;

public class StringUtil {

	public static String join(List<String> toJoin, String separator) {
		StringJoiner sj = new StringJoiner(separator);
		toJoin.forEach( s -> sj.add(s) );
		return sj.toString();
	}
	
}
