package com.kipodopik.pqj.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {

	@SafeVarargs
	public static <T> List<T> join(List<T> ...lists) {
		List<T> rv = new ArrayList<T>();
		
		for (List<T> list : lists) {
			rv.addAll(list);
		}		
		
		return rv;
	}
	
}
